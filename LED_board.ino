
// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include "LED.h"
#include <stdio.h>

//strip is array of Adafruit_NeoPixels
//useful print statement:       Serial.println(String(ir) + " " + String(c) + " " + String(sensorValue) + " " + String(lowThresh) + " " + String(highThresh));


int sensArray[IR_WIDTH * IR_HEIGHT]; //IR_WIDTH * IR_HEIGHT
int baseArray[IR_WIDTH * IR_HEIGHT];
int thresholdArray[IR_WIDTH * IR_HEIGHT];

//pong
//ball
int velBallx;
int velBally;
int ballx;
int bally;
int ballsize;
//paddle
int paddle1x;
int paddle1y;
int velPad;
int paddle2x;
int paddle2y;
int paddleHeight;
int paddleWidth;
int paddleSens[2*IR_HEIGHT];
int gameover;
int score1;
int score2;
int wait;
int speedUp;
int timer_stop;

//WhackAMole
int moleTimer;
int molex;
int moley;
int moleScore;
int lives;
int rectX;
int rectY;
int first;

//RIPPLE
int rippleArray[IR_WIDTH * IR_HEIGHT];
int rippleSensArray[IR_WIDTH * IR_HEIGHT];
int prevRipple[IR_WIDTH * IR_HEIGHT];

int replayCount;
int sensitivity;

enum modes {
  MENU = 0,
  WAVE = 1,
  RIPPLE = 2,
  PONG = 3,
  WHACK = 4,
  REPLAY = 5,
  SCREEN = 6
};

enum modes gameMode;
enum modes prevState;

int starArrayX[15];
int starArrayY[15];

void setup() {
  Serial.begin(115200);
  Serial.println("SUP");
  pinMode(muxRowIn0, OUTPUT);
  pinMode(muxRowIn1, OUTPUT);
  pinMode(muxRowIn2, OUTPUT);
  pinMode(muxRowIn3, OUTPUT);
  pinMode(muxMuxIn0, OUTPUT);
  pinMode(muxMuxIn1, OUTPUT);
  pinMode(muxMuxIn2, OUTPUT);
  pinMode(muxMuxIn3, OUTPUT);
  pinMode(muxOut0, INPUT);
  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
  pinMode(LED6, OUTPUT);
  pinMode(LED7, OUTPUT);
  pinMode(LED8, OUTPUT);
  pinMode(LED9, OUTPUT);
  pinMode(LED10, OUTPUT);
  pinMode(LED11, OUTPUT);
  stripSetup();

  //reset all LEDs to 0
  setZero();
  delay(500);
  stripShow();

  readBaseSensors(baseArray);
  delay(100);
  for (int i=0; i<NUMSTRIP; i++) {
    strip[i].begin(); strip[i].setBrightness(15);
  }
  readThresholdSensors(thresholdArray);
  for (int i=0; i<NUMSTRIP; i++) {
    strip[i].begin(); strip[i].setBrightness(BRIGHT);
  }

  gameMode = MENU;

  replayCount = REPLAY;
  sensitivity = SENSITIVITY;
  

  //PONG:
  //ball
  velBallx = BALL_VEL;
  velBally = BALL_VEL;
  ballx = 15;
  bally = 10;
  ballsize = 2;

  //paddle
  paddle1x = 2;
  paddle1y = LED_HEIGHT/2 - 4;
  paddle2x = 36;
  paddle2y = LED_HEIGHT/2 - 4;
  paddleHeight = 8;
  paddleWidth = 2;
  //velocity of paddle
  velPad = 1;
  
  //gameplay
  gameover = 0;
  score1 = 0;
  score2 = 0;
  wait = 3;
  speedUp = 0;

  //MOLE:
  moleTimer = 0;
  molex = 0;
  moley = 0;
  moleScore = 0;
  lives = LIVES;
  first = 1;
  
  for (int i = 0; i < 15; i++){
    starArrayX[i] = random(0,40);
    starArrayY[i] = random(0, 5);
  }

  timer_stop = 0;
  
  Serial.println("END CALIBRATION");
}

//call setZero() to clear old drawings
//call stripShow() to show the new drawing
void loop() {
  setZero();

  //MODES:
  switch (gameMode){
    case (MENU):
      menuInteract();
      break;
    case (WAVE):
      sensitivity = 70;
      handWave();
      sensitivity = SENSITIVITY;
      break;
    case (RIPPLE):
      sensitivity = 115;//100;
      rippleEffect();
      sensitivity = SENSITIVITY;
      break;
    case (PONG):
      sensitivity = 60;
      prevState = PONG;
      playPong();
      sensitivity = SENSITIVITY;
      break;
    case (WHACK):
      sensitivity = 90;
      prevState = WHACK;
      playWhack();
      sensitivity = SENSITIVITY;
      break;
    case (REPLAY):
      playAgainInteract();
      break;
    case (SCREEN):
      christmas();
      break;
  }

  //drawAll();
  //drawGameOver();
  //drawOutlineCircle(10, 10, 6, 255, 255, 255);
  stripShow();
}

///////////////////////////////////////////////////////////////
////////                RIPLE HANDWAVE                ////////
///////////////////////////////////////////////////////////////
void *rippleEffect(){
  if (timer_stop >= 800){
    timer_stop = 0;
    changeMode(MENU);
    replayCount = REPLAYCOUNT;
    return;
  }
  timer_stop++;
  
  readHandWaveSensors(rippleSensArray); 
   
  //iterate through all IR's
  for (int ir = 0; ir < IR_WIDTH * IR_HEIGHT; ir += 2){
    int c = sensorReading(rippleSensArray, ir);
    
   // int quit = checkExit(c, ir);
    int ripNum = rippleArray[ir];
    //if ripple is triggered and not already rippling
    if (c >= sensitivity && ripNum <= 0 && prevRipple[ir] == 0){
      ripNum = RIPPLE_DISTANCE;
      prevRipple[ir] = 1; //set to 1 if only want 1 ripple/touch
    }
    //this 'if' is for ripple when remove from sensor
    else if (c < sensitivity && ripNum <= 0 && prevRipple[ir] == 1){
      ripNum = RIPPLE_DISTANCE;
    }
    else if (c < sensitivity){
      prevRipple[ir] = 0;
    }
    if(ir == 0 && replayCount <= 0){
      changeMode(MENU);
      replayCount = REPLAYCOUNT;
      timer_stop = 0;
      return;
    }
    else if(ir == 0 && c > sensitivity){
      replayCount -= 1;
    }
    if (ripNum <= 0){
      continue;
    }
    
    int x = (ir % IR_WIDTH) * 3 + 1.5 - .5; //x coordinate
    int y = (ir / IR_WIDTH) * 3 + 1.5 - .5; //y coordinate

    int r = 0; //ir%5 * 50;
    int g = 0; //ir%100 * 2;
    int b = 255; //ir%10 * 20;
    if (ripNum == RIPPLE_DISTANCE){
      drawRectangle(x, y, 2, 2, r, g, b);
    }
    else{
      drawOutlineCircle(x, y, RIPPLE_DISTANCE - ripNum + 1, r, g, b); 
      drawOutlineCircle(x, y, RIPPLE_DISTANCE - ripNum , 0, 0, 0);
    }
    rippleArray[ir] = ripNum - 1;
  }
  drawX(0, 0, 255, 0, 0);
}


///////////////////////////////////////////////////////////////
////////                 WHACK A MOLE                  ////////
///////////////////////////////////////////////////////////////

void playWhack(){
  if (moleTimer <= 0){
    lives -= 1;
    int oldx = rectX;
    int oldy = rectY;
    if(first != 1){
      drawLives();
      drawScore(0,0, moleScore);
      drawRectangle(oldx, oldy, MOLE_SIZE * 2, MOLE_SIZE * 2, 255, 0, 0);
      stripShow();
      delay(300);
      setZero();
    }
    if (lives <= 0){
      drawGameOver();
      drawScore(15,18, moleScore);
      stripShow();
      delay(5000);
      //for next time
      setZero();
      moleScore = 0;
      first = 1;
      moleTimer = 0;
      lives = LIVES;
      changeMode(REPLAY);
      return;
    }
    resetMoleLoc();
    first = 0;
    return;
  }
  int hit = readWhackSensors();
  if (hit){
    moleScore += 1;
    resetMoleLoc();
    return;
  }
  moleTimer -= 1;
  drawPepe(rectX, rectY);
  drawScore(0,0, moleScore);
  drawLives();
}

void *resetMoleLoc(){
  int moleFirst = 1;
  while(moleFirst || molex == 1 && moley == 3){
    molex = random(1, 12);
    moley = random(1, 7);
    moleFirst = 0;
  }

  double coord[2];
  coord[0] = molex * 3 + 1.5; //x coordinate
  coord[1] = moley * 3 + 1.5; //y coordinate

  //top left x-coordinate of desired square
  rectX = 1 + coord[0]- MOLE_SIZE -.5; 
  rectY = 1 + coord[1] - MOLE_SIZE -.5; 
  
  //drawRectangle(rectX, rectY, MOLE_SIZE * 2, MOLE_SIZE * 2, 0, 0, 255);
  drawPepe(rectX, rectY);
  moleTimer = MOLE_RESET - (moleScore/2);
  if (moleTimer <= 0){
    moleTimer = 5;
  }
  drawLives();
  drawScore(0,0, moleScore);
}

void drawScore(int x, int y, int score){
  if(score < 10){
    drawNumber(x, y, 0, 255, 140, 0);
    drawNumber(x+4, y, score, 255, 140, 0);
  }
  else if(score < 100){
      drawNumber(x,y, score/10, 255, 140, 0);
      drawNumber(x+4, y, score%10, 255, 140, 0);
  }
  else if(score < 1000){
    drawNumber(x, y, score/100, 255, 140, 0);
    drawNumber(x+4,y, (score % 100) / 10, 255, 140, 0);
    drawNumber(x+8, y, score%10, 255, 140, 0);
  }
}

void drawLives(){
  int x = 32;
  int y = 1;
  for (int i = 0; i < lives; i++){
    int actualX = x + (i*3);
    drawCircle(actualX, 1, 1, 255, 0, 0);
  }
  return;
}


//return 1 for hit, 0 for miss
int readWhackSensors(){
  int loc = molex + moley*13;
  int sensorValue = readSensors(loc);

  int c = sensorReading(sensorValue, loc);
  
  if (c <= sensitivity)
    return 0;
  return 1;
}


///////////////////////////////////////////////////////////////
////////                   PONG                        ////////
///////////////////////////////////////////////////////////////
void playPong(){
  //score
  drawNumber(16, 2, score1, 255, 140, 0);
  drawNumber(22, 2, score2, 255, 140, 0);
  //line through middle
  colSet(LED_WIDTH/2, 0, 24, 255, 105, 180);
  
  //if ball went behind a paddle
  if(gameover > 0){
    drawRectangle(ballx, bally, ballsize, ballsize, 255,0,0);
    drawRectangle(paddle1x, paddle1y, paddleWidth, paddleHeight, 0, 0, 255);
    drawRectangle(paddle2x, paddle2y, paddleWidth, paddleHeight, 0, 255, 0);
    //draw W on correct side
    if (score1 == 10 || score2 == 10){
      if(score1 == 10){
        drawLetter(8, 9, 'W', 255, 140, 0);
        drawLetter(LED_WIDTH/2 + 5, 9, 'L', 255, 0, 0);
        drawNumber(12, 2, 1, 255, 140, 0);
        drawNumber(16, 2, 0, 255, 140, 0);
      }
      else{
        drawLetter(LED_WIDTH/2 + 5, 9, 'W', 255, 140, 0);
        drawLetter(8, 9, 'L', 255, 0, 0);
        drawNumber(21, 2, 1, 255, 140, 0);
        drawNumber(25, 2, 0, 255, 140, 0);
      }
      stripShow();
      delay(3000);
      changeMode(REPLAY);
      //ball
      velBallx = BALL_VEL;
      velBally = BALL_VEL;
      ballx = 15;
      bally = 10;
      ballsize = 2;
    
      //paddle
      paddle1x = 2;
      paddle1y = LED_HEIGHT/2 - 4;
      paddle2x = 36;
      paddle2y = LED_HEIGHT/2 - 4;
      paddleHeight = 8;
      paddleWidth = 2;
      //velocity of paddle
      velPad = 1;
      
      //gameplay
      gameover = 0;
      score1 = 0;
      score2 = 0;
      wait = 3;
      speedUp = 0;
      return;
    }
    stripShow();
    delay(300);
    //start ball on side of loser
    if(gameover == 1){
      ballx = random(4, LED_WIDTH/2 - 5);
      bally = random(4, LED_HEIGHT/2);
      velBallx = BALL_VEL;
      velBally = BALL_VEL;
    }
    else{
      ballx = random(LED_WIDTH/2 + 5, 35);
      bally = random(4, LED_HEIGHT/2);
      velBallx = -BALL_VEL;
      velBally = BALL_VEL;
    }

    //if someone has reached 10
    if (score1 == 10 || score2 == 10){
      delay(5000);
      //reset score
      score1 = 0;
      score2 = 0;
      
      //remove W from correct side
      if(score1 == 10){
        drawLetter(8, 9, 'W', 0, 0, 0);
      }
      else{
        drawLetter(LED_WIDTH/2 + 5, 9, 'W', 0, 0, 0);
      }
    }
    
    //reset speedUP
    speedUp = 0;
  }
  
  //double speed if game has gone SPEEDUP_TIME cycles
  //1st: 2 x 1
  //2nd: 2 x 2
  //3rd: 1 x 2
  if(speedUp == SPEEDUP_TIME){
    velBallx = velBallx * 2;
    velBally = velBally;
  }
  else if(speedUp == 2*SPEEDUP_TIME){
    velBallx = velBallx;
    velBally = velBally * 2;
  }
  else if(speedUp == 3*SPEEDUP_TIME){
    velBallx = velBallx/2;
    velBally = velBally;
  }
  speedUp += 1;
  
  gameover = movingBall();
  movingPaddle();
}

//controls the paddle
void movingPaddle(){
  drawRectangle(paddle1x, paddle1y, paddleWidth, paddleHeight, 0, 0, 255);
  drawRectangle(paddle2x, paddle2y, paddleWidth, paddleHeight, 0, 255, 0);

  readPongSensors(paddleSens); 
    
  int vel1 = 0;
  int vel2 = 0;

  //go through IR sensors on the paddles
  for (int ir = 0; ir < IR_HEIGHT; ir++){
    int paddle1 = ir * IR_WIDTH;
    int paddle2 = ir * IR_WIDTH + (IR_WIDTH - 1);
    vel1 += setVel(paddle1, ir, paddle1y);
    vel2 += setVel(paddle2, ir + IR_HEIGHT, paddle2y);
  }

  //if vel1 or vel2 is positive, this means the paddle needs to move down
  //if vel1 or vel2 is negative, this means the paddle needs to move up
  //if 0, both up and down sensors were equally active, and the paddle shouldn't move
  
  if (vel1 > 0 && (paddle1y + velPad + paddleHeight) < LED_HEIGHT + 1){
    paddle1y += velPad;
  }
  else if (vel1 < 0 && (paddle1y - velPad) >= 0){
    paddle1y -= velPad;
  }

  if (vel2 > 0 && (paddle2y + velPad + paddleHeight) < LED_HEIGHT + 1){
    paddle2y += velPad;
  }
  else if (vel2 < 0 && (paddle2y - velPad) >= 0){
    paddle2y -= velPad;
  }
}

//controls the ball
int movingBall(){
  int gameover = 0;
  drawRectangle(ballx, bally, ballsize, ballsize, 255,255,255);
  
  ballx += velBallx;
  bally += velBally;
  //if right side of board
  if (ballx + ballsize > LED_WIDTH-1){
    ballx = LED_WIDTH-ballsize;
    velBallx = -velBallx;
    score1 += 1;
    gameover = 2;
  }
  //if left side of board
  if (ballx <= 0){
    ballx = 0;
    velBallx = -velBallx;
    score2 += 1;
    gameover = 1;
  }
  //if top of board
  if (bally <= 0){
    bally = 0;
    velBally = -velBally;
  }
  //if bottom of board
  if (bally + ballsize > LED_HEIGHT-1){
    bally = LED_HEIGHT-ballsize;
    velBally = -velBally;
  }
  //if recent collision, wait before checking again
  if(wait > 0){
    wait -= 1;
    return gameover;
  }
  
  //corner of paddle2
  if((ballx + ballsize == paddle2x && bally + ballsize == paddle2y) || (ballx + ballsize == paddle2x && bally == paddle2y + paddleHeight)){
    velBallx = -velBallx;
    velBally = -velBally;
    wait = 2;
  }
  //side of paddle2
  else if (ballx + ballsize > paddle2x && ballx <= paddle2x + paddleWidth - 1 && (bally == paddle2y + paddleHeight || bally + ballsize == paddle2y)){
    velBally = -velBally;
    wait = 2;
  }
  //normal paddle1
  else if(ballx + ballsize >= paddle2x && ballx + ballsize <= paddle2x + paddleWidth && paddle2y <= bally + ballsize && bally <= paddle2y + paddleHeight){
    velBallx = -velBallx;
    wait = 2;
  }
  //corner of paddle1
  if(paddle1x + paddleWidth == ballx && (paddle1y == bally + ballsize || paddle1y + paddleHeight == bally)){
    velBallx = -velBallx;
    velBally = -velBally;
    wait = 2;
  }
  //side of paddle1
  else if(ballx < paddle1x + paddleWidth && ballx + ballsize > paddle1x && (paddle1y == bally + ballsize || paddle1y + paddleHeight == bally)){
    velBally = -velBally;
    wait = 2;
  }
  //normal paddle1
  else if(ballx <= (paddle1x + paddleWidth) && ballx >= paddle1x && bally + ballsize > paddle1y && (bally) < paddle1y + paddleHeight){
    velBallx = -velBallx;
    wait = 2;
  }
  return gameover;
}

//if given sensor is active, return 1 or -1 to indicate that paddle should be influenced to move down or up
//ir is location of ir in terms of entire array (for calibrated value)
//sensLoc is location of ir in terms of the paddle array
//paddleY is the y coordinate of the paddle
int setVel(int ir, int sensLoc, int paddleY){
  int sensorValue = paddleSens[sensLoc];
  int c = sensorReading(sensorValue, ir);
  if (c <= sensitivity)
    return 0;
    
  double ycoord = (ir / IR_WIDTH) * 3 + 1.5;
  int halfLength = paddleHeight/2;

  if (paddleY < ycoord - halfLength +.5){
    return 1;
  }
  else if (paddleY > ycoord - halfLength +.5){
    return -1;
  }
  else{
    return 0;
  }
}

void readPongSensors(int inputArray[]){
  for (int current = 0; current < IR_HEIGHT; current++){
    //sensor for paddle 1 and paddle 2
    int paddle1 = current * IR_WIDTH;
    int paddle2 = current * IR_WIDTH + (IR_WIDTH - 1);

    int curr = paddle1;
    for(int i = 0; i < 2; i++){
      //if i = 0, get info for paddle 1
      if (i == 1){
        curr = paddle2;
      }
      int sensorValue = readSensors(curr);
      if (i == 0)
        inputArray[current] = sensorValue;
      else
        inputArray[current + IR_HEIGHT] = sensorValue;
    }
  }
}


////////////////////////////////////////////////////////////////
////////                 HAND WAVE                     ////////
///////////////////////////////////////////////////////////////

//main handwave function
void handWave(){
  if (timer_stop >= 800){
    timer_stop = 0;
    changeMode(MENU);
    replayCount = REPLAYCOUNT;
    return;
  }
  timer_stop++;
  
  readHandWaveSensors(sensArray); 
    
  int desired_spread = 3; //radius of how many pixels should be affected by each sensor.
  int distSmallest = 1; //smallest distance from any IR to first LED

  //iterate through all IR's
  for (int ir = 0; ir < IR_WIDTH * IR_HEIGHT; ir++){
    int c = sensorReading(sensArray, ir);

    if (c <= sensitivity)
      continue;
    if(ir == 0 && replayCount <= 0){
      changeMode(MENU);
      replayCount = REPLAYCOUNT;
      timer_stop = 0;
      return;
    }
    else if(ir == 0){// && c > 200){
      replayCount -= 1;
    }

    //coordinate of IR sensor
    double coord[2];
    coord[0] = (ir % IR_WIDTH) * 3 + 1.5; //x coordinate
    coord[1] = (ir / IR_WIDTH) * 3 + 1.5; //y coordinate

    //top left x-coordinate of desired square
    int initX = 1 + coord[0]-desired_spread-.5; 
    for (int x = initX; x < initX + (desired_spread * 2); x++){
      //if outside the board
      if (x < 0 || x > LED_WIDTH - 1){ continue;}
      double distX = abs(coord[0]-x);

      //top left y-coordinate of desired square
      int initY = 1 + coord[1] - desired_spread-.5; 
      for (int y = initY; y < initY + (desired_spread * 2); y++){
        //if outside the board
        if (y < 0 || y > LED_HEIGHT - 1){ continue;}
        
        //LED brightness
        double distY = abs(coord[1]-y);
        double dist = distX + distY;
        double ratio = (distSmallest)/(dist*2);
        // if(ratio > 1)
        // ratio = 1;;
        int pixelBright = c * ratio;
        
        //location on strip
        int stripNo = y / DAISY;
        int flatLoc = (y % DAISY) * LED_WIDTH + x;
        
        //add intensity to overlapped sensor readings
        int32_t pixColor = pointGet(flatLoc, stripNo);
        int perfC = pixelBright + (uint8_t) pixColor;

        if (perfC > 255) perfC = 255;
        pointSet(flatLoc, stripNo, 0, 0, perfC);
      }
    }
  }
  drawX(0, 0, 255, 0, 0);
}

//read sensor during execution of the hand wave
void* readHandWaveSensors(int inputArray[]){
   for (int current = 0; current < IR_WIDTH * IR_HEIGHT; current++){ //IR_WIDTH * IR_HEIGHT
    inputArray[current] = readSensors(current);  
  }
}

///////////////////////////////////////////////////////////////
////////                 CHRISTMAS                     ////////
///////////////////////////////////////////////////////////////
//void christmas(){
//  if(checkTouch()){
//    changeMode(MENU);
//    return;
//  }
//  
//  delay(200);
//  setZero();
//  Serial.println("HERE");
//
//  drawLetter(5, 5, "G", 255, 215, 0);
//  drawLetter(5, 20, "O", 255, 215, 0); 
//  drawLetter(20, 5, "B", 255, 215, 0);
//  drawLetter(20, 15, "E", 255, 215, 0);
//  drawLetter(20, 25, "A", 255, 215, 0);
//  drawLetter(20, 35, "R", 255, 215, 0);
//  drawLetter(20, 45, "S", 255, 215, 0);
//}

int checkTouch(){
  int countSensors = 4;
  for (int ir = 0; ir < IR_WIDTH * IR_HEIGHT; ir++){
    int c = sensorReading(readSensors(ir), ir);
    if (c > sensitivity){
      countSensors--;
    }
  }
  if (countSensors < 0){
    return 1;
  }
  return 0;
}
//upside down christmas tree
void christmas(){
  if(checkTouch()){
    changeMode(MENU);
    return;
  }
  
  delay(200);
  setZero();

  //snow bottom, blue background
  drawRectangle(0,13, 40, 11, 70, 70, 70);
  drawRectangle(0, 0, 40, 13, 0, 0, 50);
  
  //trees
  for (int i = 0; i < 15; i++){
    drawCircle(starArrayX[i], starArrayY[i], 0, 255, 255, 0);
  }
  for (int i = 0; i < 1; i++){
    int changex = 0;
    int y = random(0, 24);
    int r;
    int g;
    int b;
    int randint = random(0, 1);
    if (randint == 0){
      r = 0;
      g = 250;
      b = 0;
    }
    else if (randint == 1) {
      r = 250;
      g = 0;
      b = 0;
    }
    else{
      r = 250;
      g = 250;
      b = 250;
    }
    int x = 20 + changex;
    y = 5;
    drawTriangle(x, y, 2, r, g, b);
    drawTriangle(x, y+2, 3, r, g, b);
    drawTriangle(x, y+4, 4, r, g, b);
    drawRectangle(x-1, y+9, 3, 3, 139, 69, 19);

    x += 12;
    y += 4;
    drawTriangle(x, y, 2, r, g, b);
    drawTriangle(x, y+2, 3, r, g, b);
    drawTriangle(x, y+4, 4, r, g, b);
    drawRectangle(x-1, y+9, 3, 3, 139, 69, 19);

    x -= 27;
    y -= 1;
    drawTriangle(x, y, 2, r, g, b);
    drawTriangle(x, y+2, 3, r, g, b);
    drawTriangle(x, y+4, 4, r, g, b);
    drawRectangle(x-1, y+9, 3, 3, 139, 69, 19);
  }
  
  
  for (int i = 0; i < 20; i++){
    int x = random(0, 40);
    int y = random(0, 15);
    int r;
    int g;
    int b;
    int randint = random(0, 1);
    if (randint == 0){
      r = 255;
      g = 250;
      b = 250;
    }
    else if (randint == 1) {
      r = 0;
      g = 250;
      b = 0;
    }
    else{
      r = 250;
      g = 250;
      b = 250;
    }
    drawCircle(x, y, 0, r, g, b);
  }
}

///////////////////////////////////////////////////////////////
////////                   RANDOM                      ////////
///////////////////////////////////////////////////////////////
void testeasy(){
  int r = random(0,255);
  int b = random(0,255);
  int g = random(0,255);
  
  for (int y=0; y<NUMSTRIP; y++) {
    for (int i=0; i<NUMPIXELS; i++) {
      pointSet(i,y, r,g,b);
    }
  }
}

///////////////////////////////////////////////////////////////
////////             GENERAL FUNCTIONS                 ////////
///////////////////////////////////////////////////////////////

//SETUP FUNCTIONS:
void stripSetup() {
  for (int i=0; i<NUMSTRIP; i++) {
    strip[i].begin(); strip[i].setBrightness(BRIGHT);
  }
  randomSeed(analogRead(A15));
}

//to read the base sensor value. Takes the average value of avg reads.
void *readBaseSensors(int inputArray[]){
  int avg = 10;
  int first = 1;
  for (int numTimes = 0; numTimes < avg; numTimes++){
     for (int current = 0; current < IR_WIDTH * IR_HEIGHT; current++){ //IR_WIDTH * IR_HEIGHT
      if (first){
        inputArray[current] = readSensors(current);
        delay(5);
        continue;
      }
      inputArray[current] = min(inputArray[current], readSensors(current));
      //inputArray[current] += readSensors(current);      
      delay(5);
    }
    if (first) first = 0;
  }
//  for (int findavg = 0; findavg < IR_WIDTH * IR_HEIGHT; findavg++){
//    inputArray[findavg] = inputArray[findavg]/avg;
//  }
}

//to read the upper bound sensor value. Takes the average value of avg reads.
void *readThresholdSensors(int inputArray[]){
  setOn(255, 255, 255);
  stripShow();
  int avg = 10;
  for (int numTimes = 0; numTimes < avg; numTimes++){
     for (int current = 0; current < IR_WIDTH * IR_HEIGHT; current++){ //IR_WIDTH * IR_HEIGHT
      inputArray[current] = max(inputArray[current], readSensors(current));
     // inputArray[current] += readSensors(current);      
      delay(5);
    }
  }
//  for (int findavg = 0; findavg < IR_WIDTH * IR_HEIGHT; findavg++){
//    inputArray[findavg] = inputArray[findavg]/avg;
//  }
  setZero();
  stripShow();  
}

//ALL OTHER FUNCTIONS:
//"prints" to board
void stripShow() {
  for (int i=0; i<NUMSTRIP; i++) {
    strip[i].show();
  }
}

//turns all LEDs off
void setZero(){
    for (int y=0; y<NUMSTRIP; y++) {
      for (int i=0; i<NUMPIXELS; i++) {
        pointSet(i,y,ZERO);
      }
    }
}

//turns entire board white
void setOn(int r, int g, int b){
    for (int y=0; y<NUMSTRIP; y++) {
      for (int i=0; i<NUMPIXELS; i++) {
        pointSet(i,y,r, g, b);
      }
    }
}

//LED point set
void pointSet(int x, int y, int32_t color){
  strip[y].setPixelColor(x, color);
}

//LED point set
void pointSet(int x, int y, int r, int g, int b) {
  strip[y].setPixelColor(x, strip[y].Color(r,g,b));
}

//LED point get
uint32_t pointGet(int x, int y){
  return strip[y].getPixelColor(x);
}

//converts 2D LED matrix (x,y) representation to arduino representation
int *coordToPixelArray(int x, int y, int *flatLocation, int *stripNumber){
  *stripNumber = y / DAISY;
  *flatLocation = (y % DAISY) * LED_WIDTH + x;
}

//draws line from (x,y) to (x + len, y) with specified rgb color
void rowSet(int x, int y, int len, int r, int g, int b){
  int *flatLocation;
  int *stripNumber;
  for(int i = x; i < x + len; i++){
    coordToPixelArray(i, y, flatLocation, stripNumber);
    pointSet(*flatLocation, *stripNumber, r, g, b);
  }
}

//draws line from (x,y) to (x, y + len) with specified rgb color
void colSet(int x, int y, int len, int r, int g, int b){
  int *flatLocation;
  int *stripNumber;
  for(int i = y; i < y + len; i++){
    coordToPixelArray(x, i, flatLocation, stripNumber);
    pointSet(*flatLocation, *stripNumber, r, g, b);
  }
}

//reads the value of the IR sensor at position pos
//pos is the sensor number start with 0 at top left, 1 on the right of 0, etc
int readSensors(int pos){
    int rowPos = pos % IR_WIDTH;
    bool firstIn = (rowPos & 1);
    bool secondIn = (rowPos & 2);
    bool thirdIn = (rowPos & 4);
    bool fourthIn = (rowPos & 8);

    int muxPos = pos / IR_WIDTH;
    bool firstMux = (muxPos & 1);
    bool secondMux = (muxPos & 2);
    bool thirdMux = (muxPos & 4);

    digitalWrite(muxRow[0], firstIn);
    digitalWrite(muxRow[1], secondIn);
    digitalWrite(muxRow[2], thirdIn);
    digitalWrite(muxRow[3], fourthIn);

    digitalWrite(muxMux[0], firstMux);
    digitalWrite(muxMux[1], secondMux);
    digitalWrite(muxMux[2], thirdMux);
    digitalWrite(muxMux[3], LOW);

    int sensorValue = analogRead(muxOut0);
    return sensorValue;
}

int sensorReading(int sensorArray[], int ir){
    int sensorValue = sensorArray[ir];
    //calibration value for ir, lower bound
    int lowThresh = baseArray[ir];
    //upper bound for read value
    int highThresh = thresholdArray[ir];
    
    int c = map(sensorValue, lowThresh, highThresh, 0, 255);  // resistor value 3.3Kohm
    if (sensorValue <= lowThresh){
      c = 0;
    }
    else if (sensorValue >= highThresh){
      c = 255;
    }
    //Serial.println(String(ir) + " " + String(c) + " " + String(sensorValue) + " " + String(lowThresh) + " " + String(highThresh));
    return c;
}

int sensorReading(int sensorValue, int ir){
    //calibration value for ir, lower bound
    int lowThresh = baseArray[ir];
    //upper bound for read value
    int highThresh = thresholdArray[ir];
    
    int c = map(sensorValue, lowThresh, highThresh, 0, 255);  // resistor value 3.3Kohm
    if (sensorValue <= lowThresh){
      c = 0;
    }
    else if (sensorValue >= highThresh){
      c = 255;
    }
    //Serial.println(String(ir) + " " + String(c) + " " + String(sensorValue) + " " + String(lowThresh) + " " + String(highThresh));
    return c;
}
///////////////////////////////////////////////////////////////
////////               SHAPES & NUMBERS                ////////
///////////////////////////////////////////////////////////////

void *drawPepe(int x, int y){
  rowSet(x+1,y,2,0,255,0);
  rowSet(x+5,y,2,0,255,0);
  rowSet(x,y+1,7,0,255,0);
  rowSet(x,y+2,7,0,255,0);
  rowSet(x,y+3,7,0,255,0);
  rowSet(x+1,y+4,1,0,255,0);
  rowSet(x+2,y+4,5,255,0,0);
  rowSet(x+1,y+5,5,0,255,0);
  rowSet(x+5,y+2,1,255,255,255);
  rowSet(x+3,y+2,1,0,0,50);
  rowSet(x+2,y+2,1,255,255,255);
  rowSet(x+6,y+2,1,0,0,50);
  
}

void *drawX(int x, int y, int r, int g, int b){
  rowSet(x, y, 1, r, g, b);
  rowSet(x+1, y+1, 1, r, g, b);
  rowSet(x+2, y+2, 1, r, g, b);
  rowSet(x, y+2, 1, r, g, b);
  rowSet(x+1, y+1, 1, r, g, b);
  rowSet(x+2, y, 1, r, g, b);
}

void *drawRectangle(int x, int y, int width, int height, int r, int g, int b) {//, int32_t pixColor){
  for (int i = x; i < LED_WIDTH && i < x + width && i >= 0; i++){
    for (int j = y; j < LED_HEIGHT && j < y + height && j >= 0; j++){
      int *flatLocation;
      int *stripNumber;
      coordToPixelArray(i, j, flatLocation, stripNumber);
      pointSet(*flatLocation, *stripNumber, r, g, b);
    }
  }
}

void *drawOutlineRectangle(int x, int y, int width, int height, int r, int g, int b) {//, int32_t pixColor){
  colSet(x, y, height, r, g, b);
  colSet(x + width - 1, y, height, r, g, b);
  rowSet(x, y, width, r, g, b);
  rowSet(x, y + height - 1, width, r, g, b);
}

void *drawCircle(int centerX, int centerY, int radius, int r, int g, int b){//, int32_t pixColor){
  for (int j = -radius; j <= radius; j++){
    for (int i= -radius; i <= radius; i++){
      if (i*i + j*j <= radius *radius){
        if (centerX + i > LED_WIDTH-1 || centerX + i < 0 || centerY + j > LED_HEIGHT-1 || centerY + j < 0){
          continue;
        }
        int *flatLocation;
        int *stripNumber;
        coordToPixelArray(centerX + i, centerY + j, flatLocation, stripNumber);
        pointSet(*flatLocation, *stripNumber, r, g, b);
      }
    }
  }
}

void *drawOutlineCircle(int centerX, int centerY, int radius, int r, int g, int b){//, int32_t pixColor){
  for (int j = -radius; j <= radius; j++){
    for (int i= -radius; i <= radius; i++){
      if (i*i + j*j <= radius*radius && i*i + j*j >= (radius-1) * (radius-1)){
        if (centerX + i > LED_WIDTH-1 || centerX + i < 0 || centerY + j > LED_HEIGHT-1 || centerY + j < 0){
          continue;
        }
        int *flatLocation;
        int *stripNumber;
        coordToPixelArray(centerX + i, centerY + j, flatLocation, stripNumber);
        pointSet(*flatLocation, *stripNumber, r, g, b);
      }
    }
  }
}

void *drawTriangle(int x, int y, int sideLength, int r, int g, int b){
  int width = 1;
  int begx = x;
  int begy = y;
  for (int i = 0; i <= sideLength; i++){
    for (int j = begx; j < begx + width; j++){
        if (j > LED_WIDTH-1 || j < 0 || begy > LED_HEIGHT-1 || begy < 0){
          continue;
        }
        int *flatLocation;
        int *stripNumber;
        coordToPixelArray(j, begy, flatLocation, stripNumber);
        pointSet(*flatLocation, *stripNumber, r, g, b);
    }
    begy += 1;
    begx -= 1;
    width += 2;
  }
}

void drawNumber(int x, int y, int num, int r, int g, int b){
  int numwidth = 3;
  int numheight = 5;
  switch(num){
    case 0: 
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);  
      colSet(x, y, numheight, r, g, b);
      colSet(x+2, y, numheight, r, g, b);
      break;
    case 1:
      colSet(x+2, y, numheight, r, g, b);
      break;
    case 2:
      colSet(x+2, y, 2, r, g, b);
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);
      colSet(x, y+2, numwidth, r, g, b);
      break;
    case 3:
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);
      colSet(x+2, y, numheight, r, g, b);
      break;
    case 4:
      rowSet(x, y+2, numwidth, r, g, b);
      colSet(x, y, 2, r, g, b);
      colSet(x+2, y, 5, r, g, b);
      break;
    case 5:
      colSet(x, y, 2, r, g, b);
      colSet(x+2, y+2, numwidth, r, g, b);
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);
      break;
    case 6:
      colSet(x, y, 2, r, g, b);
      colSet(x+2, y+2, numwidth, r, g, b);
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);
      colSet(x, y+2, 3, r, g, b);
      break;
    case 7:
      rowSet(x, y, 3, r, g, b);
      colSet(x+2, y, 5, r, g, b);
      break;
    case 8:
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      rowSet(x, y+4, numwidth, r, g, b);
      colSet(x, y, 5, r, g, b);
      colSet(x+2, y, 5, r, g, b);
      break;
    case 9:
      rowSet(x, y, numwidth, r, g, b);
      rowSet(x, y+2, numwidth, r, g, b);
      colSet(x, y, 3, r, g, b);
      colSet(x+2, y, 5, r, g, b);
      break;
  }
}

void drawAll(){
  int r = 0;
  int g = 255;
  int b = 255;
  drawLetter(0,0,'A',r, g, b);
  drawLetter(5,0,'B',r, g, b);
  drawLetter(10,0,'C',r, g, b);
  drawLetter(15,0,'D',r, g, b);
  drawLetter(20,0,'E',r, g, b);
  drawLetter(25,0,'F',r, g, b);
  drawLetter(30,0,'G',r, g, b);
  drawLetter(35,0,'H',r, g, b);
  drawLetter(0,6,'I',r, g, b);
  drawLetter(5,6,'J',r, g, b);
  drawLetter(10,6,'K',r, g, b);
  drawLetter(15,6,'L',r, g, b);
  drawLetter(20,6,'M',r, g, b);
  drawLetter(25,6,'N',r, g, b);
  drawLetter(30,6,'O',r, g, b);
  drawLetter(35,6,'P',r, g, b);
  drawLetter(0,12,'Q',r, g, b);
  drawLetter(5,12,'R',r, g, b);
  drawLetter(10,12,'S',r, g, b);
  drawLetter(15,12,'T',r, g, b);
  drawLetter(20,12,'U',r, g, b);
  drawLetter(25,12,'V',r, g, b);
  drawLetter(5,18,'W',r, g, b);
  drawLetter(30,12,'X',r, g, b);
  drawLetter(35,12,'Y',r, g, b);
  drawLetter(0,18,'Z',r, g, b);
}
void drawLetter(int x, int y, char letter, int r, int g, int b) {
  int letterWidth =  4;
  int letterHeight = 5;
  switch (letter) {
    case 'A':
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      colSet(x, y+1, 4, r, g, b);
      colSet(x+3, y+1, 4, r, g, b);
      break;
    case 'B':
      colSet(x, y, 5, r, g, b);
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      rowSet(x+3, y+1, 1, r, g, b);
      rowSet(x+3, y+3, 1, r, g, b);
      break;
    case 'C':
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x+1, y+4, 3, r, g, b);
      colSet(x, y+1, 3, r, g, b);
      break;
    case 'D':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y+1, 3, r, g, b);
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      break;
    case 'E':
      colSet(x, y, 5, r, g, b);
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x+1, y+2, 3, r, g, b);
      rowSet(x+1, y+4, 3, r, g, b);
      break;
    case 'F':
      colSet(x, y, 5, r, g, b);
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      break;
    case 'G':
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x+2, y+2, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      colSet(x, y+1, 3, r, g, b);
      colSet(x+3, y+3, 1, r, g, b);
      break;
    case 'H':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y, 5, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      break;
    case 'I':
      colSet(x+2, y, 5, r, g, b);
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x+1, y+4, 3, r, g, b);
      break;
    case 'J':
      colSet(x+3, y, 4, r, g, b);
      colSet(x, y+2, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      break;
    case 'K':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y+3, 2, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      rowSet(x+2, y+1, 1, r, g, b);
      rowSet(x+3, y, 1, r, g, b);
      break;
    case 'L':
      colSet(x, y, 5, r, g, b);
      rowSet(x+1, y+4, 3, r, g, b);
      break;
    case 'M':
      colSet(x, y, 5, r, g, b);
      colSet(x+4, y, 5, r, g, b);
      colSet(x+1, y+1,1, r, g, b);
      colSet(x+2, y+2, 1, r, g, b);
      colSet(x+3, y+1, 1, r, g, b);
      break;
    case 'N':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y, 5, r, g, b);
      rowSet(x+1, y+1, 1, r, g, b);
      rowSet(x+2, y+2, 1, r, g, b);
      break;
    case 'O':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y, 5, r, g, b);
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      break;
    case 'P':
      colSet(x, y, 5, r, g, b);
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      rowSet(x+3, y+1, 1, r, g, b);
      break;
    case 'Q':
      colSet(x, y, 4, r, g, b);
      colSet(x+2, y, 4, r, g, b);
      rowSet(x+1, y, 1, r, g, b);
      rowSet(x+1, y+3, 1, r, g, b);
      rowSet(x+3, y+4, 1, r, g, b);
      break;
    case 'R':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y+3, 2, r, g, b);
      rowSet(x+1, y, 2, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      rowSet(x+3, y+1, 1, r, g, b);
      break;
    case 'S':
      rowSet(x+1, y, 3, r, g, b);
      rowSet(x, y+2, 4, r, g, b);
      rowSet(x, y+4, 3, r, g, b);
      colSet(x, y+1, 1, r, g, b);
      colSet(x+3, y+3, 1, r, g, b);
      break;
    case 'T':
      rowSet(x, y, 4, r, g, b);
      colSet(x+2, y+1, 4, r, g, b);
      break;
    case 'U':
      colSet(x, y, 5, r, g, b);
      colSet(x+3, y, 5, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      break;
    case 'V':
      colSet(x, y, 4, r, g, b);
      colSet(x+3, y, 3, r, g, b);
      rowSet(x+1, y+4, 1, r, g, b);
      rowSet(x+2, y+3, 1, r, g, b);
      break;
    case 'W':
      colSet(x, y, 5, r, g, b);
      colSet(x+4, y, 5, r, g, b);
      colSet(x+1, y+3, 1, r, g, b);
      colSet(x+2, y+2, 1, r, g, b);
      colSet(x+3, y+3, 1, r, g, b);
      break;
    case 'X':
      rowSet(x, y, 1, r, g, b);
      rowSet(x+1, y+1, 1, r, g, b);
      rowSet(x+2, y+2, 1, r, g, b);
      rowSet(x+3, y+3, 1, r, g, b);
      rowSet(x+4, y+4, 1, r, g, b);
      rowSet(x, y+4, 1, r, g, b);
      rowSet(x+1, y+3, 1, r, g, b);
      rowSet(x+3, y+1, 1, r, g, b);
      rowSet(x+4, y, 1, r, g, b);
      break;
    case 'Y':
      colSet(x, y, 3, r, g, b);
      colSet(x+3, y, 5, r, g, b);
      rowSet(x+1, y+2, 2, r, g, b);
      rowSet(x+1, y+4, 2, r, g, b);
      break;
    case 'Z':
      rowSet(x, y, 4, r, g, b);
      rowSet(x, y+4, 4, r, g, b);
      colSet(x, y+3, 1, r, g, b);
      colSet(x+1, y+2, 1, r, g, b);
      colSet(x+2, y+1, 1, r, g, b);
      break;
  }
}

///////////////////////////////////////////////////////////////
////////               GAME OVER & MENU                ////////
///////////////////////////////////////////////////////////////

void *changeMode(enum modes mode){
  gameMode = mode;
  gameMode = mode;
}

void *drawMenu(){
  // OUTLINE
  int white = 60;
  drawOutlineRectangle(0,0,40,24,white,white,white);
  rowSet(1,11,38,white,white,white);
  rowSet(1,12,38,white,white,white);
  colSet(19,0,24,white,white,white);
  colSet(20,0,24,white,white,white);
  
  // PONG
  drawRectangle(2,14,2,5,0,0,255);
  drawRectangle(16,17,2,5,0,255,0);
  drawRectangle(10,18,2,2,255,255,255);
  
//  // WHACK A PEPE
//  rowSet(7,3,2,0,255,0);
//  rowSet(11,3,2,0,255,0);
//  rowSet(6,4,7,0,255,0);
//  rowSet(6,5,7,0,255,0);
//  rowSet(6,6,7,0,255,0);
//  rowSet(7,7,1,0,255,0);
//  rowSet(8,7,5,255,0,0);
//  rowSet(7,8,5,0,255,0);
//  rowSet(8,5,1,255,255,255);
//  rowSet(9,5,1,0,0,50);
//  rowSet(11,5,1,255,255,255);
//  rowSet(12,5,1,0,0,50);

  drawPepe(6, 3);
  
  // HAND
  drawRectangle(24,2,8,8,0,0,255);
  colSet(27,3,2,250,140,0);
  rowSet(26,4,3,250,140,0);
  drawRectangle(25,5,4,4,250,140,0);
  drawRectangle(25, 8, 1, 1, 0, 0, 255);
  rowSet(30,5,1,250,140,0);
  rowSet(29,6,1,250,140,0);
  
  // RIPPLE
  drawOutlineCircle(29,18,1,0,0,255);
  drawOutlineCircle(29,18,4,0,0,255);
  //drawOutlineCircle(29,18,5,0,0,255);
}

void *drawGameOver(){
  int r = 255;
  int g = 0;
  int b = 0;
  
  // GAME
   int space = 2;
  drawLetter(7,3,'G',r,g,b);
  drawLetter(12 + space, 3, 'A',r,g,b);
  drawLetter(17 + 2*space, 3, 'M',r,g,b);
  drawLetter(23 + 3*space, 3, 'E',r,g,b);
  
  // OVER
  drawLetter(7,11,'O',r,g,b);
  drawLetter(12 + space,11,'V',r,g,b);
  drawLetter(17 + 2*space,11,'E',r,g,b);
  drawLetter(22 + 3*space,11,'R',r,g,b);
}

void *drawPlayAgain(){
  setOn(0, 0, 10);
  int space = 2;
   int r = 250;
   int g = 140;
   int b = 0;

  int rr = 50;
  int rg = 50;
  int rb = 50;
  // Rectangles for "buttons"
  drawOutlineRectangle(0,0,40,13,rr, rg, rb);
 // drawRectangle(1,1,38,10,0,0,0);
  drawOutlineRectangle(0,12,40,12,rr, rg, rb);
  //drawRectangle(1,14,38,9,0,0,0);
  
  // PLAY
  drawLetter(7,1,'P',r,g,b);
  drawLetter(12 + space,1,'L',r,g,b);
  drawLetter(17 + 2*space,1,'A',r,g,b);
  drawLetter(22 + 3*space,1,'Y',r,g,b);
  
  // AGAIN
  drawLetter(4,7,'A',r,g,b);
  drawLetter(9 + space,7,'G',r,g,b);
  drawLetter(14 + 2 * space,7,'A',r,g,b);
  drawLetter(18 + 3*space,7,'I',r,g,b);
  drawLetter(24 + 4*space,7,'N',r,g,b);
  
  // MENU
  drawLetter(7,16,'M',r,g,b);
  drawLetter(13 + space,16,'E',r,g,b);
  drawLetter(18 + 2*space,16,'N',r,g,b);
  drawLetter(23 + 3*space,16,'U',r,g,b);
}

void *playAgainInteract(){
  drawPlayAgain();
  stripShow();
  int sens = 4;
  int count = 0;
  while(1){
    count = 0;
    for (int ir = 0; ir < IR_WIDTH * IR_HEIGHT; ir++){
      //read value for ir
      int sensorValue = readSensors(ir);
      //calibration value for ir, lower bound
      int lowThresh = baseArray[ir];
      //upper bound for read value
      int highThresh = thresholdArray[ir];
      
      int c = map(sensorValue, lowThresh, highThresh, 0, 255);  // resistor value 3.3Kohm
      if (sensorValue < lowThresh){
        c = 0;
      }
      else if (sensorValue > highThresh){
        c = 255;
      }
      if (c <= sensitivity)
        continue;
  
      if (ir <= 51){
        count++;
      }
      else{
        count--;
      }
    }
     if(count > sens){
        changeMode(prevState);
        return;
      }
      else if (count < -sens){
        changeMode(MENU);
        return;
      }
  }
  return;
}

void *menuInteract(){
  timer_stop = 0;
  drawMenu();
  stripShow();
  delay(2000);
  int sens = 4;
  int countTop = 0;
  int countBottom = 0;
  int menuTime = 300;
  while(1){
    countTop = 0;
    countBottom = 0;
    if (menuTime == 0){
      changeMode(SCREEN);
      return;
    }
    menuTime--;
    for (int ir = 0; ir < IR_WIDTH * IR_HEIGHT; ir++){
      //read value for ir
      int sensorValue = readSensors(ir);
      //calibration value for ir, lower bound
      int lowThresh = baseArray[ir];
      //upper bound for read value
      int highThresh = thresholdArray[ir];
      
      int c = map(sensorValue, lowThresh, highThresh, 0, 255);  // resistor value 3.3Kohm
      if (sensorValue < lowThresh){
        c = 0;
      }
      else if (sensorValue > highThresh){
        c = 255;
      }
      if (c <= sensitivity)
        continue;
      int irx = ir % 13;
      int iry = ir / 13;
      if (irx < 6 && iry < 5){
        countTop++;
      }
      else if(irx < 13 && iry < 5){
        countTop--;
      }
      else if(irx < 6){
        countBottom++;
      }
      else{
        countBottom--;
      }
    }
     if(countTop > sens){
        changeMode(WHACK);
        return;
      }
      else if (countTop < -sens){
        changeMode(WAVE);
        return;
      }
           if(countBottom > sens){
        changeMode(PONG);
        return;
      }
      else if (countBottom < -sens){
        changeMode(RIPPLE);
        return;
      }
  }
  return;
}

