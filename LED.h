#define LED0            13
#define LED1            12
#define LED2            11
#define LED3            10
#define LED4            9
#define LED5            8
#define LED6            7
#define LED7            6
#define LED8            5
#define LED9            4
#define LED10           3
#define LED11           2
#define muxMuxIn0       31
#define muxMuxIn1       33
#define muxMuxIn2       35
#define muxMuxIn3       37
#define muxRowIn0       47
#define muxRowIn1       49
#define muxRowIn2       51
#define muxRowIn3       53
#define muxOut0        A0  

// How many NeoPixels are attached to the Arduino?
 #define NUMPIXELS      80//24

 // Number of Strips
 #define NUMSTRIP       12//4//2
 #define DAISY          2//3

 #define COLOR          random(0,255),random(0,255),random(0,255)
 #define BRIGHT         20//100
 #define ZERO           0,0,0
 #define LED_WIDTH      40//7
 #define LED_HEIGHT     24//8//6
 #define IR_WIDTH       13//2
 #define IR_HEIGHT      8//2 //8//2//2
 #define SENSITIVITY    150

 #define MOLE_RESET     50//50
 #define MOLE_SIZE      3
 #define BALL_VEL       1
 #define SPEEDUP_TIME   150
 #define LIVES          4 //1 + # you want
 #define RIPPLE_DISTANCE 10
 #define REPLAYCOUNT     20

 // When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
 // Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
 // example for more information on possible values.
 Adafruit_NeoPixel pixels0 = Adafruit_NeoPixel(NUMPIXELS, LED0, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels1 = Adafruit_NeoPixel(NUMPIXELS, LED1, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels2 = Adafruit_NeoPixel(NUMPIXELS, LED2, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels3 = Adafruit_NeoPixel(NUMPIXELS, LED3, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels4 = Adafruit_NeoPixel(NUMPIXELS, LED4, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels5 = Adafruit_NeoPixel(NUMPIXELS, LED5, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels6 = Adafruit_NeoPixel(NUMPIXELS, LED6, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels7 = Adafruit_NeoPixel(NUMPIXELS, LED7, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels8 = Adafruit_NeoPixel(NUMPIXELS, LED8, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels9 = Adafruit_NeoPixel(NUMPIXELS, LED9, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels10 = Adafruit_NeoPixel(NUMPIXELS, LED10, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel pixels11 = Adafruit_NeoPixel(NUMPIXELS, LED11, NEO_GRB + NEO_KHZ800);
 Adafruit_NeoPixel strip[] = {pixels0,pixels1,pixels2,pixels3,pixels4,pixels5,pixels6,pixels7,pixels8,pixels9,pixels10,pixels11};
// //Adafruit_NeoPixel strip[] = {pixels0,pixels1, pixels2, pixels3};
 int muxRow[] = {muxRowIn0, muxRowIn1, muxRowIn2, muxRowIn3};
 int muxMux[] = {muxMuxIn0, muxMuxIn1, muxMuxIn2, muxMuxIn3};
